import { useMemo } from "react"

// Свой Хук (вынесли всю догику с апп в отдельный файл)
export const useSortedPosts = (posts, sort) => {

   // сортированые посты (Хук useMemo вызывается только тогда когда меняется зависимость в [])
   const sortedPosts = useMemo(() => {
      // если в отсортированых что то есть то показываем отсортированые если нет то обычные посты
      if (sort) {
         return [...posts].sort((a, b) => a[sort].localeCompare(b[sort]))
      }
      return posts
   }, [sort, posts])

   return sortedPosts

}

export const usePosts = (posts, sort, query) => {

   const sortedPosts = useSortedPosts(posts,sort)

   // поиск в инпуте
   const sortedAndSearchedPosts = useMemo(() => {
      return sortedPosts.filter(post => post.title.toLowerCase().includes(query))
   }, [query, sortedPosts])

return sortedAndSearchedPosts;

}