import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import PostService from "../API/PostService";
import Loader from "../component/UI/loader/Loader";
import { useFetching } from "../hooks/useFetching";


const PostIdPage = () => {

   // для Получения id
   const params = useParams()

   const [post, setPost] = useState({})
   const [comments, setComments] = useState([])

   // Запрос на сервак созданым хуком
   const [fetchPostById, isLoading, error] = useFetching(async (id) => {
      const response = await PostService.getById(params.id)
      setPost(response.data)
   });
   // Запрос на сервак созданым хуком (коментарии)
   const [fetchComments, isComLoading, comError] = useFetching(async (id) => {
      const response = await PostService.getCommentsByPostId(params.id)
      setComments(response.data)
   });

   useEffect(() => {
      fetchPostById(params.id)
      fetchComments(params.id)
   }, [])

   return (
      <div style={{textAlign: 'center'}}>
         <h1 style={{color: 'wheat'}}>
            You open page ID = {params.id}
         </h1>
         {isLoading
            ? <Loader />
            : <div style={{color: 'cornsilk'}}>
               {post.id}. {post.title}
            </div>
         }
         <h1 style={{color: 'wheat'}}>
            Comments
         </h1>
         {isComLoading
            ? <Loader />
            : <div>
               {comments.map((comm, index)=>
                  <div key={index} style={{ marginTop: 15 }}>
                     <h5 style={{color: 'wheat'}}>{comm.email}</h5>
                     <div style={{color: 'cornsilk'}}>{comm.body}</div>
                  </div>

               )}
            </div>
         }

      </div>
   )
}

export default PostIdPage;