
import React, { useEffect, useMemo, useRef, useState } from "react";
import PostService from "../API/PostService";
import PostFilter from "../component/PostFilter";
import PostForm from "../component/PostForm";
import PostList from "../component/PostList";
import MyButton from "../component/UI/button/MyButton";
import Loader from "../component/UI/loader/Loader";
import MyModal from "../component/UI/modal/MyModal";
import Pagination from "../component/UI/pagination/Pagination";
import { useFetching } from "../hooks/useFetching";
import { usePosts } from "../hooks/usePosts";
import { getPageCount, getPagesArray } from "../utils/pages";




function Posts() {

  const [posts, setPosts] = useState([]) // Хук (локальный стейт (посты))
  const [filter, setFilter] = useState({ sort: '', query: '' }) // Хук (обьеденяет в себе стейт сортировки и подиска)
  const [modal, setModal] = useState(false) // Хук (модалка)
  const [totalPages, setTotalPages] = useState(0)// Хук (Количество постов)
  const [limit, setLimit] = useState(10)// Хук (лимит обьектов на страницу)
  const [page, setPage] = useState(1)// Хук (номер страницы)

  // Свой хук с папки
  const [fetchPosts, isPostsLoading, postError] = useFetching(async () => {
    const response = await PostService.getAll(limit, page)
    setPosts(response.data)
    const totalCount = response.headers['x-total-count']
    setTotalPages(getPageCount(totalCount, limit))
  })

  // Хук Жизненого цикла запрос данных
  useEffect(() => {
    fetchPosts()
  }, [page])

  const sortedAndSearchedPosts = usePosts(posts, filter.sort, filter.query)// Свой хук с папки

  // функции обратного вызова - добавляем пост (ожидает данные из дочернего элемента)
  const createPost = (newPost) => {
    setPosts([...posts, newPost])
    setModal(false)
  }
  // - удаляем пост (ожидает данные из дочернего элемента)
  const removePost = (post) => {
    setPosts(posts.filter(p => p.id !== post.id))
  }

  const changePage = (page) => {
    setPage(page)

  }

  return (
    <div className="App">
      <MyButton style={{ marginTop: 30 }} onClick={() => setModal(true)}>
        Create post
      </MyButton>
      <MyModal
        visible={modal}
        setVisible={setModal}>
        <PostForm create={createPost} />
      </MyModal>
      <hr style={{ margin: '15px 0' }} />
      <PostFilter
        filter={filter}
        setFilter={setFilter} />
      {postError && <h1>ERROR - {postError}</h1>}
      {isPostsLoading
        ?
        <div style={{ display: 'flex', justifyContent: 'center', marginTop: 50 }}>
          <Loader />
        </div>
        :
        <PostList
          remove={removePost}
          posts={sortedAndSearchedPosts}
          title={"Post List JS"} />
      }
      <Pagination
        page={page}
        changePage={changePage}
        totalPages={totalPages} />

    </div>
  );
}

export default Posts;
