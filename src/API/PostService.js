import axios from "axios"
// Запрос на сервак
export default class PostService {
   static async getAll(limit = 10, page = 1) {
      const response = await axios.get("https://jsonplaceholder.typicode.com/posts", {
         // Json сам поймет что мы организовуем пагинацию
         params: {
            _limit: limit,
            _page: page
         }
      })
      return response
   }

   static async getById(id) {
      const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      return response;
   }

   static async getCommentsByPostId(id) {
      const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
      return response;
   }
}