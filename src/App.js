import React, { useEffect, useState } from "react";
import { BrowserRouter } from "react-router-dom";
import AppRouter from "./component/AppRouter";
import Navbar from "./component/UI/navbar/Navbar";
import { AuthContext } from "./context";

import './styles/App.css'

function App() {

const [isAuth, setIsAuth] = useState(false);

// делаем проверку если в локалсторейдж есть данные то не выбрасываем опять на страницу Login
useEffect(() => {
if (localStorage.getItem('auth')) {
  setIsAuth(true)
}
}, [])

  return (
    <AuthContext.Provider value={[isAuth,setIsAuth]}>
      <BrowserRouter>
        <Navbar />
        <AppRouter />
      </BrowserRouter>
    </AuthContext.Provider>
  )
}

export default App;
