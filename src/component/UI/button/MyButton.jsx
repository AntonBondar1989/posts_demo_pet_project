import React from "react";
import classes from "./MyButton.module.css"

const MyButton = ({children, ...props}) => {
   return (
      <button {...props} className={classes.myBtn}>
         {/* специальный пропс для того что бы передавать текст */}
         {children}
      </button>
   )
}

export default MyButton;