import React, { useState } from "react";
import MyButton from "./UI/button/MyButton";
import MyInput from "./UI/input/MyInput";


const PostForm = (props) => {

   // Хук (меняем импут(title) и (body))
   const [title, setTitle] = useState('')
   const [body, setBody] = useState('')

   // Добавляем посты
   const addNewPost = (e) => {
      // что бы страница не перезагружалась по клику
      e.preventDefault()
      const newPost = {
         id: Date.now(),//айдиншик присваеваем благодаря дате
         title,
         body
      }
      props.create(newPost)
      // очищаем инпуты
      setTitle('')
      setBody('')

   }

   return (

      <form>
         {/* Управляемый компонент */}
         <MyInput
            type="text"
            placeholder="Name post"
            value={title}
            onChange={e => setTitle(e.target.value)} />
         <MyInput
            type="text"
            placeholder="Description post"
            value={body}
            onChange={e => setBody(e.target.value)} />
         <MyButton onClick={addNewPost}>Add Post</MyButton>
      </form>

   )
}

export default PostForm;