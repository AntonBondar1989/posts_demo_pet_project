import React, { useContext } from "react";
import { Route, Routes } from "react-router-dom";
import { AuthContext } from "../context";
import About from "../pages/About";
import Login from "../pages/Login";
import PostIdPage from "../pages/PostIdPage";
import Posts from "../pages/Posts";


const AppRouter = () => {

   const[isAuth, setIsAuth] = useContext(AuthContext)
   
   return (

      isAuth
         ?
         <Routes>
            <Route path="/*" element={<Posts />} />
            <Route path="/about" element={<About />} />
            <Route exact path="/posts/:id" element={<PostIdPage />} />
            <Route exact path="/posts" element={<Posts />} />
         </Routes>
         :
         <Routes>
            <Route exact path="/*" element={<Login />} />
         </Routes>


   )
}

export default AppRouter;